# CMake generated Testfile for 
# Source directory: /home/parallels/catkin_ROS/src
# Build directory: /home/parallels/catkin_ROS/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("mymoveitexample")
subdirs("ROS-IGTL-Bridge")

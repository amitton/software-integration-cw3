#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from ros_igtl_bridge.msg import igtltransform

def getData(data):
    
    rospy.loginfo("\n%0.2f: x-coordinate\n%0.2f: y-coordinate\n%0.2f: z-coordinate",data.transform.translation.x,data.transform.translation.y,data.transform.translation.z)


def listener():

    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/IGTL_TRANSFORM_IN', igtltransform, getData)
    # Subscriber listens to the topic IGTL_Transform_IM w/ data type IGTL transform, listens to getData()

    rospy.spin()


if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass

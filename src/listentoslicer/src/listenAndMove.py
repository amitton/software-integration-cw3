#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from ros_igtl_bridge.msg import igtltransform

def getData(data):
    
    rospy.loginfo("\n%0.2f: x-coordinate\n%0.2f: y-coordinate\n%0.2f: z-coordinate",data.transform.translation.x,data.transform.translation.y,data.transform.translation.z)

    go_to_pose()


def go_to_pose():
    
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = data.transform.translation.x
    pose_goal.position.y = data.transform.translation.y
    pose_goal.position.z = data.transform.translation.z
    move_group.set_pose_target(pose_goal)

    plan = move_group.go(wait=True)
    move_group.stop()

    currentPose = move_group.get_current_pose().pose

    return all_close(pose_goal,current_pose,0.01)
    

def listener():

    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/IGTL_TRANSFORM_IN', igtltransform, getData)
    # Subscriber listens to the topic IGTL_Transform_IM w/ data type IGTL transform, listens to getData()

    rospy.spin()

